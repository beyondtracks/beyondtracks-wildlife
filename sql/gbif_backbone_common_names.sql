-- create a table of common names in English from the GBIF Backbone with a preferences
-- for Catalogue of Life names
DROP TABLE IF EXISTS bt_common_names;
CREATE UNLOGGED TABLE bt_common_names AS
  SELECT DISTINCT ON (taxon_id) taxon_id,
    initcap(vernacular_name) AS common_name
  FROM
    gbif_backbone_vernacular_name
  WHERE
    language = 'en'
  ORDER BY
    taxon_id,
    CASE source
      WHEN 'Catalogue of Life' THEN 0
      ELSE 1
    END
;
