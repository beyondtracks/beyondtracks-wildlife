DROP TABLE IF EXISTS gbif_backbone_vernacular_name;

CREATE UNLOGGED TABLE gbif_backbone_vernacular_name (
    taxon_id bigint,
    vernacular_name text,
    language text,
    country text,
    country_code text,
    sex text,
    life_stage text,
    source text
);

CREATE INDEX gbif_backbone_vernacular_name_taxon_id_idx ON gbif_backbone_vernacular_name ( taxon_id );
