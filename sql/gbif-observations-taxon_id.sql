ALTER TABLE gbif_observations ALTER COLUMN taxon_id TYPE integer USING CASE WHEN taxon_id~E'^\\d+$' THEN taxon_id::integer ELSE NULL END;
