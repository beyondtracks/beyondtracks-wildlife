DROP TABLE IF EXISTS gbif_observations;

CREATE UNLOGGED TABLE gbif_observations (
    gbif_id bigint PRIMARY KEY,
    license text,
    url text,
    rights_holder text,
    institution_code text,
    event_date text,
    event_time text,
    lat decimal,
    lng decimal,
    coordinate_uncertainty decimal,
    taxon_id text, -- source taxon (eg. iNat, etc) not all sources use numeric so initially load as text from the CSV then convert back to int later
    taxon_key int, -- GBIF taxon
    species text,

    geom geometry
)
