-- create a mapping of GBIF taxon_key to iNaturalist taxon_id

DROP TABLE IF EXISTS gbif_taxon_key_to_id;
CREATE UNLOGGED TABLE gbif_taxon_key_to_id AS
  SELECT DISTINCT ON (taxon_key) taxon_key, taxon_id FROM gbif_observations WHERE taxon_id IS NOT NULL AND institution_code = 'iNaturalist';

ALTER TABLE gbif_taxon_key_to_id ADD PRIMARY KEY (taxon_key);
