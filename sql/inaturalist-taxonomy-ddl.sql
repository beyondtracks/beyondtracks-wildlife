DROP TABLE IF EXISTS inaturalist_taxonomy;

CREATE UNLOGGED TABLE inaturalist_taxonomy (
    id bigint,
    vernacular_name text
);

CREATE INDEX inaturalist_taxonomy_id_idx ON inaturalist_taxonomy ( id );

COMMENT ON COLUMN inaturalist_taxonomy.id IS 'iNaturalist taxon ID';
