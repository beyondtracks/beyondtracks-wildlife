-- beyondtracks-wildlife occurances

DROP TABLE IF EXISTS bt_occurrence;
CREATE UNLOGGED TABLE bt_occurrence AS
    SELECT DISTINCT ON (o.gbif_id)
        o.gbif_id,
        o.taxon_id, -- iNat taxon ID
        o.taxon_key, -- GBIF taxon ID
        o.species,
        t.vernacular_name,
        o.license,
        o.url,
        o.rights_holder,
        o.institution_code,
        o.event_date::timestamp,
        o.coordinate_uncertainty,
        o.geom
    FROM
        gbif_observations o
        INNER JOIN
        (SELECT DISTINCT ON (id) *, ROW_NUMBER() OVER () AS row FROM inaturalist_taxonomy ORDER BY id, row ASC) AS t -- select the first vernacular name for each taxon
        ON (o.taxon_id = t.id);

ALTER TABLE bt_occurrence ALTER COLUMN geom TYPE geometry(POINT, 4326) USING ST_SetSRID(geom, 4326);
ALTER TABLE bt_occurrence ADD PRIMARY KEY (gbif_id);
CREATE INDEX bt_occurrence_geom_idx ON bt_occurrence USING SPGIST (geom);

COMMENT ON COLUMN bt_occurrence.taxon_id IS 'iNaturalist taxon ID';
COMMENT ON COLUMN bt_occurrence.taxon_key IS 'GBIF taxon ID';
