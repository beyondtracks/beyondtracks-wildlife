SELECT json_agg(t.taxon_id)
FROM (SELECT DISTINCT taxon_id FROM bt_occurrence WHERE taxon_id IS NOT NULL ORDER BY taxon_id) t;
