# beyondtracks-wildlife

[GBIF](https://www.gbif.org/) is a network which [collates public wildlife occurrence data](https://www.gbif.org/publisher/search) and [species information](https://www.gbif.org/dataset/d7dddbf4-2cf0-4f39-9b2a-bb099caae36c) together making it an ideal single point of access for pulling wildlife occurrence and species data into [beyondtracks.com](https://beyondtracks.com).

## GBIF Occurrence Download Filter
Since GBIF contains a large amount of data, we filter down the data export to only the data we need.

`config/gbif-occurrence-filter.json` contains that filter per the documentation at https://www.gbif.org/developer/occurrence#predicates.

- Observations only (excluding fossils, etc)
- in Australia
- with geospatial coordinates
- Animal kingdom only

`config/gbif-occurrence-filter-test.json` contains a stricter filter including only iNaturalist data from 2020 or later, it's mostly used for testing to avoid processing a huge dataset.

## GBIF Backbone

The [GBIF backbone taxonomy](http://rs.gbif.org/datasets/backbone/readme.html) provides species information and common names, originally this was used, but currently it's not used.

The [iNaturalist Taxonomy DarwinCore Archive](https://www.inaturalist.org/pages/developers) is an alternative source of common names, and this is what's currently used.

The backbone does provide some taxon photos, but many Australian taxa are missing so these photos aren't used. Instead default photos are sourced from [iNaturalist](http://inaturalist.org/).

## Directory Structure

- **build** intermediate build files
- **config** configuration files
- **sql** source sql files
- **src** executable scripts and code
- **output** final build outputs

## Schema

The main build output is a table `bt_occurrence`, here is a sample of the table.

| key | value |
|-----|-------|
| `gbif_id` | 2542915660 |
| `taxon_id` | 19196 |
| `taxon_key` | 2479925 |
| `species` | Alisterus scapularis |
| `common_name` | Australian King-Parrot |
| `license` | `CC_BY_NC_4_0` |
| `url` | https://www.inaturalist.org/observations/37153608 |
| `rights_holder` | Mary Ann Irvin |
| `institution_code` | iNaturalist |
| `event_date` | 2020-01-01 12:18:09 |
| `coordinate_uncertainty` | 10.0 |
| `geom` | 0101000020E6100000A79201A00AE66240A86E2EFEB6E740C0 |
