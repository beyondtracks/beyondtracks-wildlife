FROM debian:trixie-slim

RUN apt-get -y update && apt-get -y install time make curl wget pipx parallel unzip postgresql postgis postgresql-contrib pigz miller && rm -rf /var/lib/apt/lists/*
RUN apt-get -y update && apt-get -y install nodejs node-corepack npm && rm -rf /var/lib/apt/lists/* && corepack enable && yarn set version 4.5.0
RUN pipx ensurepath && pipx install b2 && rm -rf /root/.cache/pip*
ENV PATH="$PATH:/root/.local/bin"
