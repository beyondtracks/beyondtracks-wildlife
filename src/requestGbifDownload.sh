#!/bin/bash

mkdir -p build
curl --output build/gbif_request.txt --user "$GBIF_USERNAME:$GBIF_PASSWORD" -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d @$1 https://api.gbif.org/v1/occurrence/download/request
