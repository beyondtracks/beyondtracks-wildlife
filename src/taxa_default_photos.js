#!/usr/bin/env node

import yargs from 'yargs/yargs'
import fs from 'node:fs'
import inat from 'inaturalistjs'
import { chunk } from 'lodash'
import Bottleneck from 'bottleneck'

const argv = yargs(process.argv.silce(2)
  .option('input', { type: 'string' })
  .option('ouput', { type: 'string' })
  .option('verbose', { type: 'boolean' })
  .argv

const taxonIDs = JSON.parse(fs.readFileSync(argv.input ? argv.input : 'output/taxon_ids.json'))

// request photos for 30 taxa per request
// https://api.inaturalist.org/v1/docs/#!/Taxa/get_taxa_id is
// unclear what the limits are per request, but because we don't handle
// pagination, we'd prefer this not to be too large
const chunks = chunk(taxonIDs, 30)

const limiter = new Bottleneck({
  minTime: 2000, // 1 request every 2 seconds
  maxConcurrent: 2 // limit to one request at a time
})

const photos = []

console.log(`Requesting ${taxonIDs.length} taxa over ${chunks.length} chunks`)

const promises = chunks.map((chunk, i) => {
  const promise = new Promise((resolve, reject) => {
    limiter.schedule(() => inat.taxa.fetch(chunk, {}))
      .then( res => {
        console.log(`Chunk ${i + 1} of ${chunks.length}`)
        if (res.total_results <= res.per_page) {
          // then no more pages
        } else {
          // more results on subsequent pages
          console.log('got >1 page')
        }
        res.results.forEach(taxon => {
          if (argv.verbose && taxon && taxon.id) {
            console.log(taxon.id)
          }

          if (taxon && taxon.default_photo && taxon.default_photo.license) {
            // unlicensed photos are not able to be reused so these are excluded
            const photo = {
              taxon_id: taxon.id,
              id: taxon.default_photo.id,
              source: 'iNaturalist',
              url: taxon.default_photo.medium_url,
              attribution: taxon.default_photo.attribution,
              license: taxon.default_photo.license_code
            }
            photos.push(photo)
          } else if (taxon && taxon.taxon_photos) {
            // try to find a licensed photo in the other taxon photos
            const licensedTaxonPhotos = taxon.taxon_photos.filter(taxon_photo => taxon_photo.taxon_id === taxon.id && taxon_photo.photo && taxon_photo.photo.license_code)
            if (licensedTaxonPhotos.length) {
              if (argv.verbose) {
                console.log(`Default photo for ${taxon.id} was unlicensed, but found another licensed photo to use`)
              }

              const licensedTaxonPhoto = licensedTaxonPhotos[0].photo
              const photo = {
                taxon_id: taxon.id,
                id: licensedTaxonPhoto.id,
                source: 'iNaturalist',
                url: licensedTaxonPhoto.medium_url,
                attribution: licensedTaxonPhoto.attribution,
                license: licensedTaxonPhoto.license_code
              }
              photos.push(photo)
            } else {
              if (argv.verbose) {
                console.log(`Default photo for ${taxon.id} was unlicensed, but could not find another licensed photo`)
              }
            }
          }
        })
        resolve()
      })
      .catch( err => {
        console.error(err)
        reject()
      })
  })
  return promise
})

Promise.all(promises)
  .then(values => {
    fs.writeFileSync(argv.output ? argv.output : 'output/defaultPhotos.json', JSON.stringify(photos, null, 2))
  })
