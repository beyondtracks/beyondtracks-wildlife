#!/bin/bash

# the cut command can't use header fields only indexed field numbers
# mlr (miller) can cut by field name however performance testing on
# a sample revealed cut took 3.5 sec and mlr took 30 sec, so we use
# cut by looking up the column numbers for each named column

# these column headers must appear the same and the same order as the
# SQL COPY command from the Makefile load_gbif_data target

fields=""
for field in gbifID license references rightsHolder institutionCode eventDate eventTime decimalLatitude decimalLongitude coordinateUncertaintyInMeters taxonID taxonKey species; do
    index=`unzip -p build/gbif.zip occurrence.txt | head -n1 | tr '\t' '\n' | grep --line-number --line-regexp --fixed-strings "$field" | cut -d':' -f1`
    if [ -z "$fields" ]; then
        fields="$index"
    else
        fields="$fields,$index"
    fi
    # echo "$field @ $index"
done
echo $fields
