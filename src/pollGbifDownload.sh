#!/bin/sh

id=`cat build/gbif_request.txt`
wget --spider --server-response "https://api.gbif.org/v1/occurrence/download/request/$id" > /dev/null 2> /dev/null
status="$?"

while [ "$status" -ne 0 ]; do
    # download is not ready yet
    echo 'Download not ready' >&2
    sleep 10s
done

# download is ready
export GBIF_DOWNLOAD="https://api.gbif.org/v1/occurrence/download/request/$id"
echo "$GBIF_DOWNLOAD"
