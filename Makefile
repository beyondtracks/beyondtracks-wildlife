request_gbif_download: gbif-occurrence-filter.json
	./src/requestGbifDownload.sh $<

request_gbif_download_test: gbif-occurrence-filter-test.json
	./src/requestGbifDownload.sh $<

dropdb:
	dropdb --if-exists bt

createdb:
	createdb bt
	psql -c "CREATE EXTENSION postgis;" bt

# download gbif occurence data extract
build/gbif.zip:
	mkdir -p build
	wget --progress=bar:force:noscroll -O $@ ${GBIF_DOWNLOAD}

build/gbif: build/gbif.zip
	unzip -o $< -d $@

clean_gbif_zip: build/gbif.zip
	rm -f $<

load_gbif_schema: sql/gbif-ddl.sql
	psql -f $< bt

load_gbif_data: build/gbif.zip
	unzip -p $< occurrence.txt | cut --fields `./src/buildCutFields.sh` | tr -d '"' | sed 's/[ \xC2\xA0]*\t[ \xC2\xA0]*/\t/g' | psql -c "COPY gbif_observations (gbif_id, license, url, rights_holder, institution_code, event_date, event_time, lat, lng, coordinate_uncertainty, taxon_id, taxon_key, species) FROM STDIN WITH (FORMAT CSV, DELIMITER E'\t', HEADER)" bt
	@# convert taxon_id to integer or NULL if not an integer
	psql -f sql/gbif-observations-taxon_id.sql bt
	@# fill in all missing taxon_ids based on the mapping identified from all observations, unelss we can find a complete mapping of GBIF taxon IDs to iNaturalist taxon IDs then this will need to do
	psql -f sql/gbif_taxon_key_to_id.sql bt
	psql -c 'UPDATE gbif_observations SET taxon_id = gbif_taxon_key_to_id.taxon_id FROM gbif_taxon_key_to_id WHERE gbif_observations.taxon_key = gbif_taxon_key_to_id.taxon_key;' bt
	psql -c 'DROP TABLE gbif_taxon_key_to_id;' bt
	psql -c 'UPDATE gbif_observations SET geom = ST_SetSRID(ST_Point(lng, lat), 4326);' bt
	psql -c 'ALTER TABLE gbif_observations ALTER COLUMN geom TYPE geometry(POINT, 4326) USING ST_SetSRID(geom, 4326);' bt
	psql -c 'ALTER TABLE gbif_observations DROP COLUMN lat;' bt
	psql -c 'ALTER TABLE gbif_observations DROP COLUMN lng;' bt
	psql -c 'CREATE INDEX gbif_observations_taxon_id_idx ON gbif_observations (taxon_id);' bt
	@# VACUUM FULL to reclaim space from dropped columns
	psql -c 'VACUUM FULL ANALYZE gbif_observations;' bt

load_backbone_schema: sql/backbone-ddl.sql
	psql -f $< bt

load_backbone_vernacular_name_schema: sql/backbone-vernacular-name-ddl.sql
	psql -f $< bt

load_backbone_vernacular_name_data: build/backbone.zip
	unzip -p $< VernacularName.tsv | psql -c "COPY gbif_backbone_vernacular_name FROM STDIN WITH (FORMAT CSV, DELIMITER E'\t', HEADER)" bt

build/backbone.zip:
	wget --progress=bar:force:noscroll -O $@ https://rs.gbif.org/datasets/backbone/backbone-current.zip

download_backbone_from_b2:
	b2 download-file-by-name --noProgress ${B2_WILDLIFE_BUCKET} backbone.zip build/backbone.zip

build/backbone: build/backbone.zip
	unzip -q $< -d $@

clean_backbone_zip: build/backbone.zip
	rm -f $<

build/inaturalist-taxonomy.dwca.zip:
	wget --progress=bar:force:noscroll -O $@ https://www.inaturalist.org/taxa/inaturalist-taxonomy.dwca.zip

load_inaturalist_taxonomy_schema: sql/inaturalist-taxonomy-ddl.sql
	psql -f $< bt

load_inaturalist_taxonomy_data: build/inaturalist-taxonomy.dwca.zip
	unzip -p $< VernacularNames-english.csv | awk -F, 'NR == 1 || $$3 == "en"'  | mlr --csv cut -f id,vernacularName | psql -c "COPY inaturalist_taxonomy (id, vernacular_name) FROM STDIN WITH (FORMAT CSV, DELIMITER ',', HEADER)" bt

clean_inaturalist_taxonomy_zip: build/inaturalist-taxonomy.dwca.zip
	rm -f $<

load_backbone_data: backbone-current-simple.txt.gz
	echo "COPY backbone FROM PROGRAM 'zcat `pwd`/backbone-current-simple.txt.gz';" | psql bt

print_table_size: sql/table_size.sql
	psql -f $< bt

# pg_dump options were tested for the best performance (on 8CPU machine)
#   plain no compression pg_dump >
#     3s 195M
#   plain with external compression pg_dump | pigz >
#     3.7s 39M
#   custom default compression pg_dump -Fc >
#     9.5s 39M
#   custom no compression pg_dump -Fc -Z0 >
#     2.8s 202M
#   custom with external compression pg_dump -Fc -Z0 | pigz >
#     3.7s 41M
#   directory default compression pg_dump -Fd
#     8.3s 39M
#   directory no compression pg_dump -Fd -Z0
#     3.7s 195M
#   directory with external compression pg_dump -Fd -Z0 --file t && tar -cf t.tar t && cat t.tar | pigz >
#     6s 39M
#
# therefore we choose plain with external compression as it only adds about 30% overhead but reduces the file size by 80%
output/bt-wildlife.gz:
	mkdir -p output
	pg_dump --no-owner bt | pigz > $@
	ls -lah $@

output/taxon_ids.json: sql/export-taxon-ids.sql
	mkdir -p output
	psql --no-align --tuples-only --quiet -f $< bt > $@

output/defaultPhotos.json: output/taxon_ids.json
	mkdir -p output
	./src/taxa_default_photos.js --ouput=$@ --input=$<
